package com.cts.chargeback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChargebackApp {

	public static void main(String[] args) {
		SpringApplication.run(ChargebackApp.class, args);
	}
}
