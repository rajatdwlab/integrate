package com.cts.chargeback.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cts.chargeback.dao.UserDao;
import com.cts.chargeback.objects.Login;
import com.cts.chargeback.entity.User;

@Repository
public class UserDaoImpl implements UserDao {

//	@Autowired
//	private SessionFactory sessionFactory;
	
	@PersistenceContext	
	private EntityManager entityManager;
	@Override
	public User add(User user) {
//		sessionFactory.getCurrentSession().save(user);
		entityManager.persist(user);
		return user;
	}

	@Override
	public List<User> listUsers() {
//		@SuppressWarnings("unchecked")
//		TypedQuery<User> query = (TypedQuery)sessionFactory.getCurrentSession().createQuery("from User");
		TypedQuery<User> query = entityManager.createQuery("from User",User.class);
		List<User> listUser = query.getResultList();
		if(listUser.isEmpty()){
			return new ArrayList<User>();
		}
		return query.getResultList();
	}

	@Override
	public User validateUser(Login login) {
//		Session ses = sessionFactory.getCurrentSession();
//		Query qry = (TypedQuery)ses.createQuery("from User where USER_ID=:userId AND PASSWORD=:pwd");
		TypedQuery<User> query = entityManager.createQuery("from User where USER_ID=:userId AND PASSWORD=:pwd",User.class);
		query.setParameter("userId", login.getUserId());
		query.setParameter("pwd", login.getPassword());
		
		List<User> users = query.getResultList();
		System.out.println("Dao: " + users + " size: " + users.size());

		if (users.size() != 0) {
//			flag = true;
			return users.get(0);
		}

		return null;
	}

	@Override
	public List<User> listIndividualUsers() {
//		@SuppressWarnings("unchecked")
//		TypedQuery<User> query = (TypedQuery)sessionFactory.getCurrentSession().createQuery("from User where ROLE=:role");
		TypedQuery<User> query = entityManager.createQuery("from User where ROLE=:role",User.class);
		query.setParameter("role","IndividualUser");
		return query.getResultList();
	}

	@Override
	public User showUser(Integer userId) {
//		// TODO Auto-generated method stub
//		Session ses = sessionFactory.getCurrentSession();
////		ses.cr
//		Query qry = (TypedQuery)ses.createQuery("from User where USER_ID=:userId");
		TypedQuery<User> query = entityManager.createQuery("from User where USER_ID=:userId",User.class);
		query.setParameter("userId", userId);
		
		List<User> users = query.getResultList();
		System.out.println("Dao: " + users + " size: " + users.size());


		return users.get(0);
	}


}
