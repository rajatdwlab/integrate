package com.cts.chargeback.dao;

import java.util.List;

import com.cts.chargeback.entity.ChargebackData;
import com.cts.chargeback.entity.Fund;
import com.cts.chargeback.entity.ProfitData;
import com.cts.chargeback.entity.Transaction;
import com.cts.chargeback.entity.User;
import com.cts.chargeback.objects.FundUser;

public interface BankDao {

	public abstract List<Transaction> getTransactions(Long userId,String buyOrSell);

	public abstract void generateTransaction(Transaction transaction);

	public abstract void createFund(Fund fund);

	public abstract Fund enrollUser(FundUser fundUser);

	public abstract List<User> showFundUsers(Long fundId);

	public abstract List<Fund> getFundList();

	public abstract List<Transaction> getTransactionsNotSold(Long userId);

	public abstract List<ChargebackData> getChargebackData();

	public abstract List<ProfitData> getProfitData();

	public abstract ProfitData getProfitData(Long profitPercent);

	public abstract ChargebackData getChargeBack(Long chargeBackPercent);

}