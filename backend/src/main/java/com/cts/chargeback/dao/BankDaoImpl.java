package com.cts.chargeback.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cts.chargeback.entity.ChargebackData;
import com.cts.chargeback.entity.Fund;
import com.cts.chargeback.entity.ProfitData;
import com.cts.chargeback.entity.Transaction;
import com.cts.chargeback.entity.User;
import com.cts.chargeback.objects.FundUser;

@Repository
public class BankDaoImpl implements BankDao {
//
//	@Autowired
//	private SessionFactory sessionFactory;
	@PersistenceContext	
	private EntityManager entityManager;
	@Override
	public List<Transaction> getTransactions(Long userId,String buyOrSell) {
		//@SuppressWarnings("unchecked")
		//TypedQuery<Transaction> query = (TypedQuery)sessionFactory.getCurrentSession().createQuery("from Transaction where USER_ID=:userId AND TRANSACTION_TYPE=:buyOrSell");
		
		TypedQuery<Transaction> query= entityManager.createQuery("from Transaction where USER_ID=:userId AND TRANSACTION_TYPE=:buyOrSell",Transaction.class);
		query.setParameter("userId", userId);
		query.setParameter("buyOrSell", buyOrSell);
		return query.getResultList();
	}
	@Override
	public void generateTransaction(Transaction transaction) {
	//	@SuppressWarnings("unchecked")
//		TypedQuery<User> query = (TypedQuery)sessionFactory.getCurrentSession().createQuery("from User where USER_ID=:userId");
		TypedQuery<User> query = entityManager.createQuery("from User where USER_ID=:userId",User.class);
		query.setParameter("userId", transaction.getUserId());
		User user = query.getResultList().get(0);
		user.setBalance(transaction.getBalanceInDollar());
//		entityManager
		entityManager.persist(transaction);
		entityManager.flush();
//		sessionFactory.getCurrentSession().save(user);
//		sessionFactory.getCurrentSession().save(transaction);
	}
	@Override
	public void createFund(Fund fund) {
		//sessionFactory.getCurrentSession().save(fund);
		entityManager.persist(fund);
	}
	@Override
	public Fund enrollUser(FundUser fundUser) {
		Fund fund=entityManager.find(Fund.class, fundUser.getFundId());
		User user=entityManager.find(User.class,fundUser.getUserId());
		//Fund fund = sessionFactory.getCurrentSession().get(Fund.class,fundUser.getFundId());
		//User user= sessionFactory.getCurrentSession().get(User.class,fundUser.getUserId());
		user.setRole("FundUser");
		fund.getUsers().add(user);
		fund.setFundBalance(fund.getFundBalance()+user.getBalance());
		entityManager.persist(fund);
//		sessionFactory.getCurrentSession().save(fund);
		return fund;
	}
	@Override
	public List<User> showFundUsers(Long fundId) {
		//@SuppressWarnings("unchecked")
		//TypedQuery<Fund> query = (TypedQuery)sessionFactory.getCurrentSession().createQuery("from Fund where FUND_ID=:fundId");
		TypedQuery<Fund> query = entityManager.createQuery("from Fund where FUND_ID=:fundId",Fund.class);
		query.setParameter("fundId", fundId);
		List<Fund> fund=query.getResultList();
		List<User> users = new ArrayList<>();
		if(fund.size()!=0){
			users = fund.get(0).getUsers();
		}
		return users;
	}
	@Override
	public List<Fund> getFundList() {
//		@SuppressWarnings("unchecked")
//		TypedQuery<Fund> query = (TypedQuery)sessionFactory.getCurrentSession().createQuery("from Fund");
		TypedQuery<Fund> query = entityManager.createQuery("from Fund",Fund.class);
		return query.getResultList();
	}

	@Override
	public List<Transaction> getTransactionsNotSold(Long userId) {
		//@SuppressWarnings("unchecked")
		//TypedQuery<Long> queryTwo =sessionFactory.getCurrentSession().createQuery("select t.boughtId from Transaction t where  t.boughtId IS NOT NULL");
		//List<Long> boughtIds = queryTwo.getResultList();
	//	@SuppressWarnings("unchecked")
//		TypedQuery<Transaction> query =(TypedQuery) sessionFactory.getCurrentSession().createQuery("from Transaction where USER_ID=:userId AND TRANSACTION_TYPE=:buyOrSell AND TRANSACTION_ID NOT IN (select t.boughtId from Transaction t where  t.boughtId IS NOT NULL)");
		TypedQuery<Transaction> query =entityManager.createQuery("from Transaction where USER_ID=:userId AND TRANSACTION_TYPE=:buyOrSell AND TRANSACTION_ID NOT IN (select t.boughtId from Transaction t where  t.boughtId IS NOT NULL)",Transaction.class);
		query.setParameter("userId", userId);
		query.setParameter("buyOrSell", "BUY");
		//query.setParameter("boughtIds", boughtIds);
		return query.getResultList();
	}
	@Override
	public List<ChargebackData> getChargebackData() {
//		@SuppressWarnings("unchecked")
//		TypedQuery<ChargebackData> query =(TypedQuery) sessionFactory.getCurrentSession().createQuery("from ChargebackData");
		TypedQuery<ChargebackData> query =entityManager.createQuery("from ChargebackData",ChargebackData.class);
		return query.getResultList();
	}
	@Override
	public List<ProfitData> getProfitData() {
//		@SuppressWarnings("unchecked")
//		TypedQuery<ProfitData> query = (TypedQuery)sessionFactory.getCurrentSession().createQuery("from ProfitData");
		TypedQuery<ProfitData> query = entityManager.createQuery("from ProfitData",ProfitData.class);
		return query.getResultList();
	}
	@Override
	public ProfitData getProfitData(Long profitPercent) {
		// TODO Auto-generated method stub
//		@SuppressWarnings("unchecked")
//		TypedQuery<ProfitData> query = (TypedQuery)sessionFactory.getCurrentSession().createQuery("from ProfitData where lower <= :profitPercent AND upper >= :profitPercent");
		TypedQuery<ProfitData> query = entityManager.createQuery("from ProfitData where lower <= :profitPercent AND upper >= :profitPercent",ProfitData.class);
		query.setParameter("profitPercent", profitPercent);
		ProfitData pd= query.getResultList().get(0);
		return pd;
	}
	@Override
	public ChargebackData getChargeBack(Long chargeBackPercent) {
		// TODO Auto-generated method stub
		//@SuppressWarnings("unchecked")
		//TypedQuery<ChargebackData> query =(TypedQuery) sessionFactory.getCurrentSession().createQuery("from ChargebackData where lower <= :chargeBackPercent AND upper >= :chargeBackPercent");
		TypedQuery<ChargebackData> query =entityManager.createQuery("from ChargebackData where lower <= :chargeBackPercent AND upper >= :chargeBackPercent",ChargebackData.class);
		query.setParameter("chargeBackPercent", chargeBackPercent);
		ChargebackData cd= query.getResultList().get(0);
		return cd;
	}
}
