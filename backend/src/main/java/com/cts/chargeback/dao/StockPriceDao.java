package com.cts.chargeback.dao;

import java.util.List;

import com.cts.chargeback.entity.StockPrice;

public interface StockPriceDao {
	public List<StockPrice> getStockPriceList();

	public StockPrice getStockPrice(String stockName);
}