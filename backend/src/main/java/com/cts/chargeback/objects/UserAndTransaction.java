package com.cts.chargeback.objects;

import java.util.List;

import com.cts.chargeback.entity.Transaction;
import com.cts.chargeback.entity.User;

public class UserAndTransaction {
	private User user;
	private List<Transaction> transactionList;

	public UserAndTransaction(User user, List<Transaction> transactionList) {
		super();
		this.user = user;
		this.transactionList = transactionList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Transaction> getTransactionList() {
		return transactionList;
	}

	public void setTransactionList(List<Transaction> transactionList) {
		this.transactionList = transactionList;
	}

}
