package com.cts.chargeback.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="CHARGEBACK_DATA")
public class ChargebackData {
	@Id
	@Column(name = "CHARGEBACK_ID")
	private String chargebackId;
	
	@Column(name = "LOWER")
	private Long lower;
	
	@Column(name = "UPPER")
	private Long upper;
	
	@Column(name = "PERCENTAGE")
	private Double percentage;

	public String getChargebackId() {
		return chargebackId;
	}

	public void setChargebackId(String chargebackId) {
		this.chargebackId = chargebackId;
	}

	public Long getLower() {
		return lower;
	}

	public void setLower(Long lower) {
		this.lower = lower;
	}

	public Long getUpper() {
		return upper;
	}

	public void setUpper(Long upper) {
		this.upper = upper;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	
}
