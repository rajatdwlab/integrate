package com.cts.chargeback.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FUND")
public class Fund {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FUND_ID")
	private Long fundId;
	
	@Column(name = "FUND_NAME")
	private String fundName;
	
	@Column(name = "FUND_BALANCE")
	private Double fundBalance;
	
	public Double getFundBalance() {
		return fundBalance;
	}

	public void setFundBalance(Double fundBalance) {
		this.fundBalance = fundBalance;
	}

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "FUND_USER", joinColumns = { @JoinColumn(name = "FUND_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "USER_ID") })
	private List<User> users=new ArrayList<>();
	
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Long getFundId() {
		return fundId;
	}

	public void setFundId(Long fundId) {
		this.fundId = fundId;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

}
