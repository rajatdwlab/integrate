package com.cts.chargeback.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PROFIT_DATA")
public class ProfitData {

	@Id
	@Column(name = "PROFIT_ID")
	private String profitId;
	
	@Column(name = "LOWER")
	private Long lower;
	
	@Column(name = "UPPER")
	private Long upper;
	
	@Column(name = "PERCENTAGE")
	private Double percentage;

	public String getProfitId() {
		return profitId;
	}

	public void setProfitId(String profitId) {
		this.profitId = profitId;
	}

	public Long getLower() {
		return lower;
	}

	public void setLower(Long lower) {
		this.lower = lower;
	}

	public Long getUpper() {
		return upper;
	}

	public void setUpper(Long upper) {
		this.upper = upper;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	
}
