package com.cts.chargeback.service;
import java.util.List;

import com.cts.chargeback.entity.ChargebackData;
import com.cts.chargeback.entity.ProfitData;
import com.cts.chargeback.entity.Transaction;
import com.cts.chargeback.entity.User;
import com.cts.chargeback.objects.FundUser;
import com.cts.chargeback.entity.Fund;
import com.cts.chargeback.entity.StockPrice;
public interface BankService {
	List<Transaction> getTransactions(Long userId,String buyOrSell);
	List<Transaction> getTransactionsNotSold(Long userId);
	void generateTransaction(Transaction transaction);
	void createFund(Fund fund);
	Fund enrollUser(FundUser fundUser);
	List<User> showFundUsers(Long fundId);
	List<StockPrice> showStockPrice();
	List<Fund> getFundList();
	StockPrice showStockPrice(String stockName);
	List<ChargebackData> getChargeBackData();
	List<ProfitData> getProfitData();
	ProfitData getProfitData(Long profitPercent);
	ChargebackData getChargeBack(Long chargebackPercent);
}
