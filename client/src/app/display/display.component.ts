import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { Login } from '../../Pojo/login';
import { UserService } from '../user.service';
import { Transaction } from '../../Pojo/transaction';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { User } from '../../Pojo/user';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
public newUser = new User(undefined,'','','','',undefined,'','','','','','',10000);
 public newLogin =new Login(undefined,"");
  public transactionList:Transaction[];


  boughtStock(): void {
    this.userservice.boughtStockAll(this.newLogin.userId).then(transactionList=>{this.transactionList=transactionList});
  }

  soldStock(): void {
    this.userservice.soldStock(this.newLogin.userId).then(transactionList=>{this.transactionList=transactionList});
  }
  
  loginUser(login:Login): void {
        console.dir(login);
        this.userservice.loginUser(login).then(userAndTransactions=>{
         console.log(userAndTransactions);
         if(userAndTransactions.user==null)
          {
              this.router.navigateByUrl('/login');
          }
                this.newUser=userAndTransactions.user,
                this.transactionList=userAndTransactions.transactionList
            }
        );
    }

  constructor(private userservice:UserService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
    const userId = +this.route.snapshot.paramMap.get('userId');
    const password = this.route.snapshot.paramMap.get('password');
    this.newLogin.userId=userId;
    this.newLogin.password=password;
    this.loginUser(this.newLogin);
  }

}