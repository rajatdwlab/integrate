import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { UserService } from '../user.service'
import { User } from '../../Pojo/user';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

public newUser = new User(undefined,'','','','',undefined,'','','','','','',10000);
public user:User;

createUser(user: User): void {
        this.userservice.createUser(user).then(user=>{
          this.user=user;
          //console.log(user);
          //console.log(" this.user::: "+this.user);
          //console.log("userid:: "+user.userId);
          this.router.navigateByUrl('/login/'+user.userId);
        });
        
      //  this.router.navigateByUrl('/login'+this.user.userId);
    }

  constructor(private userservice:UserService,private router:Router) { }

  ngOnInit() {
  }

}
