import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { User } from '../Pojo/user';
import { Login } from '../Pojo/login';
import { UserAndTransaction} from '../Pojo/userAndTransaction';
import { Transaction } from '../Pojo/transaction';
import { Fund } from '../Pojo/Fund';
import { StockPrice } from '../Pojo/StockPrice';

@Injectable()
export class UserService {
 private headers = new Headers({ 'Content-Type': 'application/json' });
  private userUrl = 'http://localhost:8080/chargeback-services';

  createUser(user: User): Promise<User> {
    console.log('----- Service: '+user);
    return this.http
      .post(this.userUrl + "/addUser", user, { headers: this.headers })
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }


 loginUser(login: Login): Promise<UserAndTransaction> {
    console.log('----- Service: '+login);
    return this.http
      .post(this.userUrl + "/login", login, { headers: this.headers })
      .toPromise()
      .then(response => response.json() as UserAndTransaction)
      .catch(this.handleError);
  }

  loginAdmin(login: Login): Promise<User> {
    console.log('----- Service: '+login);
    return this.http
      .post(this.userUrl + "/loginSimple",login, { headers: this.headers })
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }

   getIndividualUsers(): Promise<User[]> {
    return this.http
      .get(this.userUrl + "/usersIndividual")
      .toPromise()
      .then(response => response.json() as User[])
      .catch(this.handleError);
  }

  getFundList(): Promise<Fund[]> {
    return this.http
      .get(this.userUrl + "/funds")
      .toPromise()
      .then(response => response.json() as Fund[])
      .catch(this.handleError);
  }
  boughtStockAll(userId:number) : Promise<Transaction[]> {
    return this.http
      .get(this.userUrl + "/transactions/"+userId+'/BUY',{ headers: this.headers })
      .toPromise()
      .then(response => response.json() as Transaction[])
      .catch(this.handleError);
  }
  boughtStock(userId:number) : Promise<Transaction[]> {
    return this.http
      .get(this.userUrl + "/notSold/"+userId,{ headers: this.headers })
      .toPromise()
      .then(response => response.json() as Transaction[])
      .catch(this.handleError);
  }

soldStock(userId:number) : Promise<Transaction[]> {
    return this.http
      .get(this.userUrl + "/transactions/"+userId+"/SELL",{ headers: this.headers })
      .toPromise()
      .then(response => response.json() as Transaction[])
      .catch(this.handleError);
  }

findUser(userId:number) : Promise<User> {
    return this.http
      .get(this.userUrl + "/user/"+userId,{ headers: this.headers })
      .toPromise()
      .then(response => response.json() as Transaction[])
      .catch(this.handleError);
  }

stockPrice() : Promise<StockPrice[]> {
    return this.http
      .get(this.userUrl + "/stockPrices",{ headers: this.headers })
      .toPromise()
      .then(response => response.json() as StockPrice[])
      .catch(this.handleError);
  }

commitBuyOrSell(transaction: Transaction) : Promise<Transaction>{
return this.http
      .post(this.userUrl + "/transactionEntry",transaction,{ headers: this.headers })
      .toPromise()
      .then(response => response.json() as Transaction)
      .catch(this.handleError);
}
getCurrentPrice(stock: string) : Promise<StockPrice>{
return this.http
      .get(this.userUrl + "/stockPrice/"+stock,{ headers: this.headers })
      .toPromise()
      .then(response => response.json() as StockPrice)
      .catch(this.handleError);
}


private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }



  constructor(private http:Http) { }

}
