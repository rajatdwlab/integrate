import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { Login } from '../../Pojo/login';
import { UserService } from '../user.service';
import { Transaction } from '../../Pojo/transaction';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { User } from '../../Pojo/user';
import { Fund } from '../../Pojo/Fund';

@Component({
  selector: 'app-admin-display',
  templateUrl: './admin-display.component.html',
  styleUrls: ['./admin-display.component.css']
})
export class AdminDisplayComponent implements OnInit {
  public user=new User(undefined,'','','','',undefined,'','','','','','',10000);
  public userList:User[];
  public newLogin =new Login(undefined,"");
  public fundList:Fund[];

  constructor(private userservice:UserService,private route: ActivatedRoute,private router:Router) { }

loginadmin(login:Login): void {
        console.dir(login);
        this.userservice.loginAdmin(login).then(user=>{
          if(user.password==null)
          {
            this.router.navigateByUrl('/adminlogin');
          }
          this.userservice.getIndividualUsers().then(userlist=>{this.userList=userlist});
               this.user=user;
            }
        );
    }

  fundsList(){
this.userservice.getFundList().then(fundlist=>{this.fundList=fundlist});
this.userList=null;
}
  usersList(){
  this.userservice.getIndividualUsers().then(userlist=>{this.userList=userlist});
  this.fundList=null;
  }

  ngOnInit() {
    const userId = +this.route.snapshot.paramMap.get('userId');
    const password = this.route.snapshot.paramMap.get('password');
    this.newLogin.userId=userId;
    this.newLogin.password=password;
    this.loginadmin(this.newLogin);
    //this.userservice.getIndividualUsers().then(userlist=>{this.userList=userlist});
  }

}
