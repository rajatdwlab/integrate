import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Transaction } from '../../Pojo/transaction';
import { StockPrice } from '../../Pojo/stockPrice';
import { User } from '../../Pojo/User';

@Component({
  selector: 'app-individual-trade',
  templateUrl: './individual-trade.component.html',
  styleUrls: ['./individual-trade.component.css']
})
export class IndividualTradeComponent implements OnInit {
  constructor(private userservice:UserService,private route: ActivatedRoute) {}

  public transaction=new Transaction(undefined,undefined,"",undefined,undefined,undefined,undefined,undefined,"","","",undefined);
  public stockList:StockPrice[];
  public transactionList:Transaction[];
  
  public stockListForSell:StockPrice[];
  public chargeBack:number[];
  public user:User;
  public userId:number;
  public stock:StockPrice;
  public quantities:number[];
  public currentPrices:number[];
  public chargeBackForSell:number[];

  calculateChargeBack(quantity:number,price:number,index:number){
    console.log(quantity+" q:p "+price);
    this.chargeBack[index]=quantity*price/100;
  }

  calculateChargeBackForSell(price:number,quantity:number,index:number){
    console.log(quantity+" q:p "+price);
    this.chargeBackForSell[index]=quantity*price/100;
  }

  buy(){
  this.userservice.stockPrice().then(stockList=>{this.stockList=stockList;
    this.quantities=Array(stockList.length).fill(0);
    this.chargeBack=Array(stockList.length).fill(0);
  });
  this.userservice.findUser(this.userId).then(user=>{this.user=user});
  this.transactionList=null;
  }

  sell(){
  this.userservice.boughtStock(this.userId).then(transactionList=>{this.transactionList=transactionList;
    this.currentPrices=Array(transactionList.length).fill(0);
    this.chargeBackForSell=Array(transactionList.length).fill(0);
  });
  //this.userservice.stockPrice().then(stockListForSell=>{this.stockListForSell=stockListForSell});
  this.userservice.findUser(this.userId).then(user=>{this.user=user});
  this.stockList=null;
  }

  commitBuy(stock:StockPrice,quantity:number,index:number){
  this.transaction.userId=this.user.userId;
  this.transaction.transactionType="BUY";
  this.transaction.stockPrice=stock.price;
  this.transaction.chargeBack=this.chargeBack[index];
  this.transaction.stockName=stock.name;
  this.transaction.quantity=quantity;
  this.transaction.balanceInDollar=this.user.balance-this.chargeBack[index]-stock.price*this.transaction.quantity;
  this.userservice.commitBuyOrSell(this.transaction).then(transaction=>{
      this.user.balance=transaction.balanceInDollar;
      
      this.userservice.boughtStock(this.userId).then(transactionList=>{
        
        this.transactionList=transactionList;
        this.currentPrices=Array(transactionList.length).fill(0);
        this.chargeBackForSell=Array(transactionList.length).fill(0);
      
      });
      this.stockList=null;
      
  });
  }
  commitSell(transxn:Transaction,index:number){
    console.log("commit sell called for : "+transxn.userId);
  this.transaction.userId=transxn.userId;
  this.transaction.transactionType="SELL";
  this.transaction.stockPrice=this.currentPrices[index];
  this.transaction.chargeBack=this.chargeBackForSell[index];//this.chargeBack[index];
  this.transaction.stockName=transxn.stockName;
  this.transaction.quantity=transxn.quantity;
  this.transaction.balanceInDollar=this.user.balance-this.chargeBackForSell[index]+transxn.quantity*this.currentPrices[index];
  this.transaction.boughtId=transxn.transactionId;
    this.userservice.commitBuyOrSell(this.transaction).then(
      transaction=>{
        this.user.balance=transaction.balanceInDollar;
        this.userservice.boughtStock(this.userId).then(transactionList=>{
          this.transactionList=transactionList;
          this.currentPrices=Array(transactionList.length).fill(0);
          this.chargeBackForSell=Array(transactionList.length).fill(0);
        })      
      }
    );
    
  }

  getCurrentPrice(stockName:string){
  this.userservice.getCurrentPrice(stockName).then(stockPrice=>{this.stock=stockPrice});
  }

  ngOnInit() {
    this.userId = +this.route.snapshot.paramMap.get('userId');
    this.userservice.findUser(this.userId).then(user=>{this.user=user});
    this.userservice.stockPrice().then(stockList=>{this.stockList=stockList;
      this.quantities=Array(stockList.length).fill(0);
      this.chargeBack=Array(stockList.length).fill(0);
    }
    );
  }

}
