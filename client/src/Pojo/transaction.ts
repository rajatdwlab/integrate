export class Transaction {
    constructor(public transactionId:number,public userId:number,public stockName:string,public stockPrice:number,
        public quantity:number, public chargeBack:number,public balanceInCurrency:number,public balanceInDollar:number,
        public transactionType:string, public dateTime:String,public currency:String,public boughtId:number){

        }
     
 }
