# Chargeback -> Spring Boot-1.5.0.Release + Angular 2 configured and managed by Gradle
## Authors
Rajat Surana
## Gradle
### Build war file
`gradle buildFullApplication`
### War file can be executed by from the root folder:
`java -jar  backend\build\libs\chargeback-1.0.war`
### Boot
#### All-In-One
`gradle bootFullApplication`
#### Standalone
`gradle bootStandaloneClient`
`gradle bootStandaloneBackend`
